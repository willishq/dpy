#!/usr/bin/env bash

if [ $# -gt 0 ]; then
    exec "$@"
else
    python manage.py runserver 0.0.0.0:8000
fi