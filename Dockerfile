FROM ubuntu:20.04

ARG USER_ID
ARG GROUP_ID

RUN addgroup -gid ${GROUP_ID} user
RUN adduser --disabled-password --gecos '' --uid ${USER_ID} --gid ${GROUP_ID} user
WORKDIR /app

ENV DEBIAN_FRONTEND noninteractive

ENV TZ=UTC

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update \
    && apt-get install -y gosu curl git python-is-python3 python3-pip \
    && curl -sL https://deb.nodesource.com/setup_15.x | bash - \
    && apt-get install -y nodejs \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip3 install --upgrade pip

USER user

COPY requirements.txt .

RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

COPY ./start.sh /usr/local/bin/start.sh

USER root

RUN chmod +x /usr/local/bin/start.sh

USER user

ENTRYPOINT ["start.sh"]
