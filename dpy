#!/usr/bin/env bash

export DOPY_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
export APP_SERVICE=${APP_SERVICE:-"app.dpy"}
export USER_ID="$(id -u)"
export GROUP_ID="$(id -g)"
export APP_DIR="$( pwd -P )"
EXEC="no"

_dc() {
    (docker-compose "$@")
}

_dr() {
    _dc run --rm "$@"
}

_de() {
    _dc exec "$@"
}

_run() {
    if [ $EXEC == "yes" ]; then
        _de "${APP_SERVICE}" "$@"
    else
        _dr "${APP_SERVICE}" "$@"
    fi
}

if [ $# -gt 0 ]; then
    if [ -f .env ]; then
        source ./.env
    fi

    if [ "$1" == "init" ]; then
        shift 1
        if [ -f "./docker-compose.yml" ] || [ -f "./Dockerfile" ] || [ -f "./start.sh" ]; then
            echo "please only init in a fresh directory"
            exit 1
        fi
        cp $DOPY_DIR/docker-compose.yml ./docker-compose.yml
        cp $DOPY_DIR/Dockerfile ./Dockerfile
        cp $DOPY_DIR/start.sh ./start.sh
        
        if [ ! -f "requirements.txt" ]; then
            touch ./requirements.txt
            if [ "$1" == "django" ]; then
                shift 1
                app="app"
                if [ -z "$1" ]; then
                    app="$1"
                fi
                echo "Django==3.1" > ./requirements.txt

                _dc build "${APP_SERVICE}"
                _run python -m django startproject "${app}" .
                exit 1
            fi
        fi
        _dc build "${APP_SERVICE}"

    else 
        PS="(_dc ps -q)"

        if _dc ps | grep 'Exit' &> /dev/null; then
            _dc down > /dev/null 2>&1
        elif [ -n "$PS" ]; then
            EXEC="yes"
        fi

        _CMD="$1"
        shift 1

        passthru=("node npm npx bash")

        if [ "${_CMD}" == "manage" ]; then
            _run python manage.py $@
        elif [ "${_CMD}" == "django" ]; then
            _run python -m django $@
        elif [ "${_CMD}" == "pip" ]; then
            _run pip3 $@
        elif [[ "${passthru[@]}" =~ "${_CMD}" ]]; then
            _run "${_CMD}" $@
        else
            _dc "${_CMD}" $@
        fi
    fi
fi
